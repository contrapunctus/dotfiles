My configuration files. Elevated to the public domain using the [Unlicense](https://unlicense.org/). If they help you in any way, please consider supporting me on Liberapay.

<a href="https://liberapay.com/contrapunctus/donate"><img alt="Donate using Liberapay" src="https://img.shields.io/liberapay/receives/contrapunctus.svg?logo=liberapay"></a>

