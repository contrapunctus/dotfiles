#!/bin/bash
kmonad ~/kmonad-legion.kbd &
kmonad ~/kmonad-tvs.kbd &
setxkbmap us dvorak -option ctrl:swapcaps -option compose:caps -option compose:paus &
sleep 0.5
